import unittest
from main import kaya_eqn

class TestUM(unittest.TestCase):
    def setUp(self):
        pass

    def test_correct_1(self):
        self.assertEqual(kaya_eqn(pop=82.4, gdp = 44, enInt=5, carbInt=5), 90640)

    def test_correct_2(self):
        self.assertEqual(kaya_eqn(pop=412, gdp = 44, enInt=5, carbInt=3.67, output_type="C"), 90640)

    def test_negative(self):
        self.assertRaises(ValueError, kaya_eqn, 82.4, -44, 5, 0.05)

    def test_invalid(self):
        self.assertRaises(TypeError, kaya_eqn, "GHH", 44, 5, 0.05)

    def test_invalid_output_type(self):
        self.assertRaises(ValueError, kaya_eqn, 82.4, 44, 5, 0.05, "climateChangeIsFake")

def run():
    unittest.main()

if __name__ == '__main__':
    run()