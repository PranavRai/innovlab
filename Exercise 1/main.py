

def kaya_eqn(pop: float,
             gdp: float,
             enInt: float,
             carbInt: float,
             output_type: str = "CO2"):
    """
    Computes the Kaya Identity equation

    :param pop: Population size in millions
    :param gdp: GDP per capita (in 1000$/Millions)
    :param enInt: Energy Intensity in Gigajoule/$1000 GDP
    :param carbInt: Carbon Intensity in tonnes CO2/Gigajoule
    :return:
    """
    local_vars = vars()
    for variable_name in local_vars:
        if variable_name == "output_type":
            if output_type != "CO2" and output_type != "C":
                raise ValueError("output_type argument can only be string 'CO2' or 'C'")
            continue

        if type(local_vars[variable_name]) is not float and type(local_vars[variable_name]) is not int:
            raise TypeError("Expected float, got {0} for variable {1}".format(type(local_vars[variable_name]).__name__, variable_name))
        if local_vars[variable_name] <0:
            raise ValueError("Negative value for variable %s" % variable_name)

    output = pop * gdp * enInt * carbInt
    if output_type == "C":
        output /= 3.67

    print(output_type + " Emissions= " + str(output))
    return output

if __name__ == "__main__":
    kaya_eqn(pop=82.4, gdp = 44, enInt=5, carbInt=5)

    # kaya_eqn(pop="GHH", gdp=44, enInt=5, carbInt=0.05)
    # kaya_eqn(pop=82.4, gdp=-44, enInt=5, carbInt=0.05)



